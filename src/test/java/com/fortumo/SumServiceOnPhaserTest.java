package com.fortumo;

import org.junit.jupiter.api.Test;

class SumServiceOnPhaserTest {

  private SumServiceOnPhaser service = new SumServiceOnPhaser();

  @Test
  void process() {

    new Thread(() -> {
      service.process("2", null);
    }).start();

    ConcurrencyService.sleepInMilliseconds(100);

    new Thread(() -> {
      service.process("end", null);
    }).start();

    ConcurrencyService.sleepInMilliseconds(2000);


  }
}
