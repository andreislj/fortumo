package com.fortumo;

import static java.time.Duration.ofSeconds;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTimeoutPreemptively;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.LongAdder;
import java.util.function.Consumer;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class SumServletTest {

  private HttpClient httpClient = new HttpClient(8888);
  private JettyServer jettyServer;

  @BeforeEach
  void start() throws Exception {
    jettyServer = new JettyServer();
    jettyServer.start(8888, 50, false);
    //  httpClient = new HttpClient(8888);
  }

  @AfterEach
  void stop() {
    jettyServer.stop();
  }

  @Test
  void sum4Requests() {
    sum("", 3, 6);
  }

  @Test
  void sum51Requests() {
    sum("", 50, 1275);
  }

  @Test
  void sum101Requests() {
    sum("", 100, 5050);
  }

  @Test
  void sum201Requests() {
    sum("", 200, 20100);
  }

  @Test
  void sum_30Requests_1EndRequest() throws InterruptedException {

    final int requests = 30;

    for (int i = 0; i < requests; i++) {

      final AtomicInteger number = new AtomicInteger(i);

      new Thread(() -> {
        final String sum = httpClient.makeCall(number.get());
        System.out.printf("Response Nr: %s, sum: %s%n", number, sum);
        //sumRequestsNumberFrequency.merge(Integer.parseInt(sum), 1, (o, n) -> o + 1);
      }).start();

    }

    //TODO (07.08.2021) Andrei Sljusar: all requests reached server and started waiting end request...
    Thread.sleep(1000);

    final String end = httpClient.makeCall("end");
    assertEquals(435, Integer.parseInt(end));

  }

  //final Map<Integer, Integer> sumRequestsNumberFrequency = new ConcurrentHashMap<>();
  //sumRequestsNumberFrequency.merge(Integer.parseInt(sum), 1, (o, n) -> o + 1);

  //0 -> 3
  //3 -> 3
  //end -> 3
  //1 -> 3
  //2 -> 3
  //end -> 3
  //ends: 3 + 3 = 6, frequency: 3 (sum) = 4 (responses)
  void sum(final String path, final int requestsNumber, final int expectedSum) {

    final Map<Integer, Integer> responseToSumMap = new ConcurrentHashMap<>();

    for (int i = 0; i <= requestsNumber; i++) {
      final AtomicInteger number = new AtomicInteger(i);
      makeCall("" + number, i1 -> responseToSumMap.put(number.get(), i1));
    }

    final BlockingQueue<Integer> endResponses = new ArrayBlockingQueue<>(requestsNumber);
//    final Queue<Integer> endResponses = new ConcurrentLinkedQueue<>();

    final AtomicInteger totalSum = new AtomicInteger();

    assertTimeoutPreemptively(ofSeconds(3), () -> {

      while (totalSum.get() != expectedSum) {
        makeCall("end", i -> {
          endResponses.add(i);
          totalSum.set(sum(endResponses));
        });
        ConcurrencyService.sleepInMilliseconds(25);
      }

    }, () -> String.format("Sum of all end responses should be: %s. Actual: %s", expectedSum, totalSum));

    assertTimeoutPreemptively(ofSeconds(2), () -> {
      while (responseToSumMap.size() != requestsNumber + 1) {
        ConcurrencyService.sleepInMilliseconds(25);
      }
    }, () -> String.format("Number of responses should be: %s, actual: %s", (requestsNumber + 1), responseToSumMap.size()));

    final Set<Integer> endSums = new HashSet<>(endResponses);

    Integer actualSum = 0;

    for (final Integer endSum : endSums) {
      final int count = (int) responseToSumMap.values().stream().filter(s -> s.equals(endSum)).count();
      actualSum += count;
    }

    assertEquals(requestsNumber + 1, actualSum);

  }

  private static final Logger logger = LoggerFactory.getLogger(SumServletTest.class);


  //  void makeCall(final String body, final BlockingDeque<Integer> responses) {
  void makeCall(final String body, final Consumer<Integer> r) {
    new Thread(() -> {
      final String sum = httpClient.makeCall(body);
      if (sum.equals("")) {
        logger.info(String.format("Response Nr: %s, sum: %s%n", body, "--------------------------------"));
      } else {
        logger.info(String.format("Response Nr: %s, sum: %s%n", body, sum));
        r.accept(Integer.parseInt(sum));
      }
    }, "@@@@@@@res-" + body + "@@@@@@").start();
  }

  @Test
  void _nonBlocking() {

    final BlockingDeque<Integer> numberResponses = new LinkedBlockingDeque<>();

    makeCall("5x", numberResponses::add);
    //ConcurrencyService.sleepInMilliseconds(1);

    makeCall("end", numberResponses::add);
    ConcurrencyService.sleepInMilliseconds(100);

    makeCall("2x", numberResponses::add);
    ConcurrencyService.sleepInMilliseconds(1000);

    makeCall("end", numberResponses::add);

    ConcurrencyService.sleep(7);

    assertEquals(Arrays.asList(7, 7, 7), new ArrayList<>(numberResponses));

  }

  @Test
  void nonBlocking1() {

    final BlockingDeque<Integer> numberResponses = new LinkedBlockingDeque<>();

    makeCall("5x", numberResponses::add);
    makeCall("4x", numberResponses::add);

    ConcurrencyService.sleepInMilliseconds(1000);

    makeCall("end", numberResponses::add);

    ConcurrencyService.sleep(6);

    assertEquals(Arrays.asList(9, 9, 9), new ArrayList<>(numberResponses));

  }

  @Test
  void nonBlocking2() {

    final BlockingDeque<Integer> numberResponses = new LinkedBlockingDeque<>();

    makeCall("5x", numberResponses::add);
    ConcurrencyService.sleepInMilliseconds(500);

    makeCall("end", numberResponses::add);
    ConcurrencyService.sleepInMilliseconds(500);

    makeCall("2x", numberResponses::add);
    ConcurrencyService.sleepInMilliseconds(500);

    makeCall("end", numberResponses::add);

    ConcurrencyService.sleep(4);

    assertEquals(Arrays.asList(2, 2, 5, 5), new ArrayList<>(numberResponses));

  }

  private int _sum(final Collection<LongAdder> collection) {
    return collection.stream().map(LongAdder::intValue).reduce(0, Integer::sum);
  }

  private int sum(final Collection<Integer> collection) {
    return collection.stream().reduce(0, Integer::sum);
  }

  public static void main(String[] args) {
    final BlockingQueue<Integer> q = new LinkedBlockingQueue<>();
    q.add(1);
  }

}
