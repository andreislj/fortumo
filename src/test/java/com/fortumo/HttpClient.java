package com.fortumo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HttpClient {

  private static final Logger logger = LoggerFactory.getLogger(HttpClient.class);

  private int port;

  public HttpClient(final int port) {
    this.port = port;
  }

  public String makeCall(final Integer n) {
    return makeCall("", "" + n);
  }

  public String makeCall(final String body) {
    return makeCall("", body);
  }

  public String makeCall(final String path, final String body) {

    try {
      final URL url = new URL(String.format("http://localhost:%s/%s", port, path));
      final HttpURLConnection conn = (HttpURLConnection) url.openConnection();
      conn.setRequestMethod("POST");
      conn.setDoOutput(true);
      writeBody(conn, body);
      final String sum = getResponse(conn);
      return sum;
    } catch (final IOException e) {
      throw new RuntimeException(e);
    }
  }

  private void writeBody(final HttpURLConnection conn, final String body) {

    try (final OutputStream os = conn.getOutputStream()) {
      byte[] input = body.getBytes();
      os.write(input, 0, input.length);
    } catch (final IOException e) {
      throw new RuntimeException("Can't write body: " + body, e);
    }

  }

  private String getResponse(final HttpURLConnection conn) {
    try {
      BufferedReader br;
      if (100 <= conn.getResponseCode() && conn.getResponseCode() <= 399) {
        br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
      } else {
        br = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
      }
      return br.lines().collect(Collectors.joining());
    } catch (final IOException e) {
      logger.error(e.getMessage());
      return "";
      //throw new RuntimeException(e);
    }
  }

}
