package com.fortumo;

import static com.fortumo.ConcurrencyService.await;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTimeoutPreemptively;

import java.time.Duration;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//@Disabled
class SumerTest {

  final ExecutorService es = Executors.newFixedThreadPool(10);

  private static final Logger logger = LoggerFactory.getLogger(SumerTest.class);

  final Lock lock = new ReentrantLock();
  final Condition c1 = lock.newCondition();
  final Condition c2 = lock.newCondition();
  //final Condition c = lock.newCondition();

  //  AtomicBoolean xxx = new AtomicBoolean(true);
  //volatile boolean xxx = true;

  @Test
  void sumAddNumbersAsynchronously() throws Exception {

//    final Sumer sumer = new Sumer();
    final __Sumer sumer = new __Sumer();
    final Future<Integer> future = Executors.newFixedThreadPool(1).submit(sumer);

    for (int i = 0; i <= 1000; i++) {
      final AtomicInteger ai = new AtomicInteger(i);
      es.submit(() -> sumer.add(ai.get()));
    }

    sumer.end(() -> {
      lock.lock();
      logger.info("Is about to signal.");
      c1.signal();
      logger.info("Signalled.");
      lock.unlock();
    });

    new Thread(sumer::end, "Sumer ender").start();

    lock.lock();
    await(c1, "await end");
    lock.unlock();

    final Integer sum = future.get();
    System.out.println("sum = " + sum);

    assertEquals(sumer.doneNumbers.stream().reduce(0, Integer::sum), sum, future.toString());

  }

  @Disabled
  @Test
  void sumAddNumbersSynchronously() {

    final Sumer sumer = new Sumer();

    for (int i = 0; i <= 1000; i++) {
      final AtomicInteger ai = new AtomicInteger(i);
      sumer.add(ai.get());
    }

    final Future<Integer> future = Executors.newFixedThreadPool(1).submit(sumer);

    final CountDownLatch endCondition = new CountDownLatch(1);
    //sumer.end(endCondition);
    await(endCondition);

    assertTimeoutPreemptively(Duration.ofSeconds(1), () -> {
      final Integer sum = future.get();
      //assertEquals(sumer.summedUpNumbers.stream().reduce(0, Integer::sum), sum, future.toString());
      assertEquals(500500, sum, future.toString());
    });

  }


}
