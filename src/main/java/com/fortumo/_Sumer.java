package com.fortumo;

import static com.fortumo.ConcurrencyService.await;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.LongAdder;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//TODO (07.08.2021) Andrei Sljusar: thread-safe?
public class _Sumer implements Callable<Integer> {

  private static final Logger logger = LoggerFactory.getLogger(_Sumer.class);

  //TODO (04.08.2021) Andrei Sljusar: Expected numbers are without decimal places and the sum of them will not exceed 10 billion (10^9).
  //TODO (04.08.2021) Andrei Sljusar: as soon as sum reaches: 1 000 000 000 stop?
  private final LongAdder sum = new LongAdder();

  private volatile boolean ended = false;

  //TODO (07.08.2021) Andrei Sljusar: Buffer on ConcurrentLinkedQueue

  private final BlockingQueue<Integer> numbers = new LinkedBlockingQueue<>();


  public final BlockingQueue<Integer> summedUpNumbers = new LinkedBlockingQueue<>();

  private final BlockingQueue<Future<Integer>> futures = new LinkedBlockingQueue<>();
  //TODO (10.08.2021) Andrei Sljusar: final int i = Runtime.getRuntime().availableProcessors();
  final ExecutorService threadPool = Executors.newFixedThreadPool(10);

  private int delay;

  public Condition responseBlockCondition;

  private Lock lock = new ReentrantLock();

  private Condition endSummingUpCondition = lock.newCondition();
  private CountDownLatch endCountDownLatch;

  private Condition xxx = lock.newCondition();

  public _Sumer() {
    this(0, null);
  }

  public _Sumer(final Condition condition) {
    this(0, condition);
  }

  public _Sumer(final int delay, final Condition condition) {
    this.delay = delay;
    this.responseBlockCondition = condition;
  }

  public Integer getSum() {
    return sum.intValue();
  }

  public void end(final CountDownLatch endCountDownLatch) {

    this.endCountDownLatch = endCountDownLatch;

    ended = true;

    threadPool.shutdown();
    awaitTermination(threadPool);

    sumAvailableNumbers();

    lock.lock();
    try {
      endSummingUpCondition.signal();
    } finally {
      lock.unlock();
    }

  }

  private boolean awaitTermination(final ExecutorService executorService) {
    try {
      final boolean b = executorService.awaitTermination(1, TimeUnit.MINUTES);
      logger.info("Thread pool has been terminated: " + b + ", active tasks: " + futures.stream().filter(f -> !f.isDone()).count() + ", numbers: " + numbers + ", sum = " + sum);
      return b;
    } catch (InterruptedException e) {
      throw new RuntimeException(e);
    }
  }

  private Integer sumAvailableNumbers() {
    final List<Integer> list = new ArrayList<>();
    numbers.drainTo(list);

    if (list.isEmpty()) {
      return 0;
    }

    final Integer s = list.stream().reduce(0, Integer::sum);
    this.sum.add(s);
    summedUpNumbers.addAll(list);

    logger.info(String.format("Sum of: %s = %s", list, s));

    return s;
  }

  public void add(final Integer i) {

    if (ended) {
      return;
    }

    numbers.add(i);

    threadPool.submit(this::sumAvailableNumbers);
  }

  @Override
  public Integer call() {

    logger.info("Sum calculation START: " + this);

    lock.lock();
    try {

      while (!ended || !numbers.isEmpty()) {
        await(endSummingUpCondition, this::toString);
      }

      logger.info("Sum calculation DONE: " + this);

      if (endCountDownLatch != null) {
        endCountDownLatch.countDown();
      }

      r.run();

      return getSum();
    } finally {
      lock.unlock();
    }

  }

  private Runnable r;


  @Override
  public String toString() {
    return "Sumer{" +
        "sum=" + sum +
        ", ended=" + ended +
        ", numbers=" + numbers +
        ", delay=" + delay +
        '}';
  }

  //  final static DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm:ss.SSSSS");
  final static DateTimeFormatter dtf = DateTimeFormatter.ofPattern("mm:ss.SSSSS");

  static class MessageAndAge {

    private final Integer delta;
    private final LocalDateTime created;

    private MessageAndAge(final Integer delta, final LocalDateTime created) {
      this.delta = delta;
      this.created = created;
    }

    @Override
    public String toString() {
      return "MessageAndAge{" +
          "delta=" + delta +
          ", created=" + dtf.format(created) +
          '}';
    }
  }

  static class FixtureState {

    private final BlockingQueue<MessageAndAge> deltas = new LinkedBlockingQueue<>();
    private AtomicBoolean calculating = new AtomicBoolean(false);

    public AtomicBoolean getCalculating() {
      return calculating;
    }

    private LocalDateTime getHeadCreated() {
      final MessageAndAge head = deltas.peek();
      if (Objects.isNull(head)) {
        return null;
      } else {
        return head.created;
      }
    }

    @Override
    public String toString() {
      return "FixtureState{" +
          "deltas=" + deltas +
          ", calculating=" + calculating +
          '}';
    }
  }

  static class TimeAndState implements Comparable<TimeAndState> {

    private final FixtureState fixtureState;
    private final LocalDateTime created;

    public TimeAndState(final FixtureState fixtureState, final LocalDateTime created) {
      this.fixtureState = fixtureState;
      this.created = created;
    }

    public LocalDateTime getCreated() {
      return created;
    }

    public FixtureState getFixtureState() {
      return fixtureState;
    }

    @Override
    public int compareTo(final TimeAndState o) {
      return created.compareTo(o.created);
    }

    @Override
    public String toString() {
      return "TimeAndState{" +
          "fixtureState=" + fixtureState +
          ", created=" + dtf.format(created) +
          '}';
    }
  }

  private static FixtureState fixtureState = new FixtureState();
  private final static BlockingQueue<TimeAndState> tasksQueue = new PriorityBlockingQueue<>();

  public static void main(String[] args) {

    new Thread(() -> {
      final AtomicInteger msgNumber = new AtomicInteger();
      while (true) {
        ConcurrencyService.sleep(1);
        final LocalDateTime now = LocalDateTime.now();

        final MessageAndAge messageAndAge = new MessageAndAge(msgNumber.incrementAndGet(), now);
        fixtureState.deltas.add(messageAndAge);

        final TimeAndState timeAndState = new TimeAndState(fixtureState, now);
        tasksQueue.add(timeAndState);
      }
    }).start();

    new Thread(() -> {

      while (true) {
        try {

          ConcurrencyService.sleep(2);

          final TimeAndState timeAndState = tasksQueue.take();
          System.out.println("timeAndState = " + timeAndState);

          final LocalDateTime tsTime = timeAndState.getCreated();
          final FixtureState fs = timeAndState.getFixtureState();

          final LocalDateTime fsTime = fs.getHeadCreated();

          if (Objects.nonNull(fsTime) && (tsTime.isEqual(fsTime))) {

            if (fs.getCalculating().compareAndSet(false, true)) {
              final List<MessageAndAge> readyMessages = new ArrayList<>();
              fs.deltas.drainTo(readyMessages);
              System.out.println("readyMessages = " + readyMessages);
              fs.getCalculating().set(false);

              final LocalDateTime newHeadOfQueueTimestamp = fs.getHeadCreated();

              if (Objects.nonNull(newHeadOfQueueTimestamp)) {
                final TimeAndState e = new TimeAndState(fs, newHeadOfQueueTimestamp);
                System.out.println("New = " + e);
                tasksQueue.add(e);
              }


            } else {
              System.out.println("Calculating....");
            }

          } else {
            System.out.println("Not equals -> ignore: " + timeAndState + " tsTime: " + dtf.format(tsTime) + ", fsTime: " + dtf.format(fsTime));
          }

        } catch (Exception e) {
          //log.error("Unexpected exception updating fixture state", e);
        } finally {
          //working = false;
        }
      }


    }).start();


  }


}

