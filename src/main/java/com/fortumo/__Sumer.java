package com.fortumo;

import static com.fortumo.ConcurrencyService.await;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.LongAdder;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class __Sumer implements Callable<Integer> {

  private static final Logger logger = LoggerFactory.getLogger(__Sumer.class);

  //TODO (04.08.2021) Andrei Sljusar: Expected numbers are without decimal places and the sum of them will not exceed 10 billion (10^9).
  //TODO (04.08.2021) Andrei Sljusar: as soon as sum reaches: 1 000 000 000 stop?
  private final LongAdder sum = new LongAdder();
  private volatile boolean ended = false;
  private BlockingQueue<Integer> numbers = new LinkedBlockingQueue<>();
  public final BlockingQueue<Integer> doneNumbers = new LinkedBlockingQueue<>();
  //  private ExecutorService threadPool = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
  private ExecutorService threadPool = Executors.newFixedThreadPool(20);

  private final int delay;

  public Condition responseBlockCondition;

  private final Lock lock = new ReentrantLock();
  private final Condition summingUpEndCondition = lock.newCondition();

  public __Sumer() {
    this(0, null);
  }

  public __Sumer(final Condition condition) {
    this(0, condition);
  }

  public __Sumer(final int delay, final Condition condition) {
    this.delay = delay;
    this.responseBlockCondition = condition;
  }

  public Integer getSum() {
    return sum.intValue();
  }

  public void end() {

    ended = true;
    threadPool.submit(this::sumUpAvailableNumbers);

    threadPool.shutdown();

    ConcurrencyService.awaitTermination(threadPool, 10, TimeUnit.SECONDS, this.toString());

    lock.lock();
    try {
      summingUpEndCondition.signal();
    } finally {
      lock.unlock();
    }

  }

  public void add(final Integer i) {

    if (ended) {
      return;
    }

    numbers.add(i);

    threadPool.submit(() -> {
      ConcurrencyService.sleep(delay);
      sumUpAvailableNumbers();
    });

  }

  private void sumUpAvailableNumbers() {
    final List<Integer> list = new ArrayList<>();
    numbers.drainTo(list);

    if (list.isEmpty()) {
      return;
    }

    final Integer s = list.stream().reduce(0, Integer::sum);
    this.sum.add(s);
    doneNumbers.addAll(list);

    logger.info(String.format("Sum of: %s = %s", list, s));

  }

  private Runnable endCallback;

  @Override
  public Integer call() {

    logger.info("Sum calculation START: " + this);

    lock.lock();
    try {
      while (!ended || !numbers.isEmpty() || !threadPool.isTerminated()) {
        logger.info("Before await: " + this);
        await(summingUpEndCondition, this::toString);
        logger.info("After await: " + this);
      }

      logger.info("Sum calculation DONE: " + this);

      return getSum();
    } finally {
      if (endCallback != null) {
        endCallback.run();
      }
      lock.unlock();
    }

  }

  @Override
  public String toString() {
    return "Sumer{" +
        "sum=" + sum +
        ", ended=" + ended +
        ", numbers=" + numbers +
        ", threadPool=" + threadPool +
        ", delay=" + delay +
        '}';
  }


  public void end(final Runnable r) {
    this.endCallback = r;
  }
}
