package com.fortumo;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.function.Supplier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConcurrencyService {

  private static final Logger logger = LoggerFactory.getLogger(ConcurrencyService.class);

  public static void await(final Condition condition, final String input) {
    try {
      logger.info("Start waiting: " + input);
      condition.await();
      logger.info("Stop waiting: " + input);
    } catch (InterruptedException e) {
      throw new RuntimeException(e);
    }
  }

  public static void await(final CountDownLatch countDownLatch) {
    try {
      countDownLatch.await();
    } catch (InterruptedException e) {
      throw new RuntimeException(e);
    }
  }

  public static void await(final Condition condition, final Supplier<String> msgSupplier) {
    try {
      logger.info("Start waiting..." + msgSupplier.get());
      condition.await();
      logger.info("Stop waiting." + msgSupplier.get());
    } catch (final InterruptedException e) {
      throw new RuntimeException(e);
    }
  }

  public static void sleep(final int seconds) {
    try {
      TimeUnit.SECONDS.sleep(seconds);
    } catch (final InterruptedException e) {
      throw new RuntimeException(e);
    }
  }

  public static void sleepInMilliseconds(final int milliseconds) {
    try {
      TimeUnit.MILLISECONDS.sleep(milliseconds);
    } catch (final InterruptedException e) {
      throw new RuntimeException(e);
    }
  }

  public static void awaitTermination(final ExecutorService executorService, final long timeout, final TimeUnit unit, final String msg) {
    try {
      logger.info("Thread pool is about to terminate.");
      executorService.awaitTermination(timeout, unit);
      logger.info("Thread pool has been terminated: " + msg);
    } catch (final InterruptedException e) {
      throw new RuntimeException(e);
    }
  }

  public static void await(final CyclicBarrier cyclicBarrier) {
    try {
      cyclicBarrier.await();
    } catch (InterruptedException | BrokenBarrierException e) {
      throw new RuntimeException(e);
    }
  }
}
