package com.fortumo;

import static com.fortumo.ConcurrencyService.await;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.LongAdder;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Sumer implements Callable<Integer> {

  private static final Logger logger = LoggerFactory.getLogger(Sumer.class);

  //TODO (04.08.2021) Andrei Sljusar: Expected numbers are without decimal places and the sum of them will not exceed 10 billion (10^9).
  //TODO (04.08.2021) Andrei Sljusar: as soon as sum reaches: 1 000 000 000 stop?
  private LongAdder sum = new LongAdder();

  private volatile boolean ended = false;
  private final BlockingQueue<Integer> numbers = new LinkedBlockingQueue<>();
  private final BlockingQueue<Thread> threads = new LinkedBlockingQueue<>();

  private final int delay;

  public Condition responseBlockCondition;

  private Lock lock = new ReentrantLock();
  private Condition endCondition = lock.newCondition();

  public Sumer() {
    this(0, null);
  }

  public Sumer(final Condition condition) {
    this(0, condition);
  }

  public Sumer(final int delay, final Condition condition) {
    this.delay = delay;
    this.responseBlockCondition = condition;
  }

  public Integer getSum() {
    return sum.intValue();
  }

  public void end() {

    ended = true;

    lock.lock();
    try {
      endCondition.signal();
    } finally {
      lock.unlock();
    }

  }

  public void add(final Integer i) {

    if (ended) {
      return;
    }

    numbers.add(i);

    //TODO (03.08.2021) Andrei Sljusar: can be done using ExecutorService
    final Thread t = new Thread(() -> {

      //simulate long-running task
      ConcurrencyService.sleep(delay);

      final List<Integer> list = new ArrayList<>();
      numbers.drainTo(list);
//      sum.add(list.parallelStream().reduce(0, Integer::sum));
      final Integer s = list.stream().reduce(0, Integer::sum);
      sum.add(s);

      logger.info(String.format("Sum of: %s = %s", list, s));

      //TODO (04.08.2021) Andrei Sljusar: if ended && !numbers.isEmpty()
      lock.lock();
      endCondition.signal();
      lock.unlock();

    });
    threads.add(t);
    t.start();

  }

  @Override
  public Integer call() {

    logger.info("Sum calculation START: " + this);

    lock.lock();
    try {
      while (!ended || !numbers.isEmpty() || areAliveThreads()) {
        await(endCondition, this::toString);
      }

      logger.info("Sum calculation DONE: " + this);

      return getSum();
    } finally {
      lock.unlock();
    }

  }

  private boolean areAliveThreads() {
    return threads.stream().anyMatch(Thread::isAlive);
  }

  @Override
  public String toString() {
    return "Sumer{" +
        "sum=" + sum +
        ", ended=" + ended +
        ", numbers=" + numbers +
        ", threads=" + threads +
        ", delay=" + delay +
        '}';
  }

}
