package com.fortumo;

import java.util.Deque;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.Phaser;
import java.util.concurrent.atomic.LongAdder;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import javax.servlet.ServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SumServiceOnPhaser {

  private static final Logger logger = LoggerFactory.getLogger(SumServiceOnPhaser.class);

  final Deque<PhaserSum> dq = new ConcurrentLinkedDeque<>();
  final Lock lock = new ReentrantLock();

  public void process(final String input, final ServletResponse response) {

    logger.info(input + ", " + lock);

    if ("end".equals(input)) {

      final PhaserSum last = dq.pollLast();
      logger.info("last = " + last);

      if (last != null) {
        //last.ph.register();
        //Arrives at this phaser and deregisters from it without waiting for others to arrive. Deregistration reduces the number of parties required to advance in future phases.
        last.ph.arriveAndDeregister();

        //last.ph.arriveAndAwaitAdvance();
        SumService.printResponse(response, last.sum.intValue());
      }

    } else {

      final var n = Integer.parseInt(input);

      lock.lock();
      PhaserSum current = dq.peekLast();
      logger.info(n + " -> current = " + current);

      if (current == null) {
        Phaser p = new Phaser(1);
        current = new PhaserSum(p);
        dq.add(current);
      }
      lock.unlock();

      current.sum.add(n);
      current.ph.register();

      ConcurrencyService.sleep(n);

      //Arrives at this phaser and awaits others.
      current.ph.arriveAndAwaitAdvance();
      SumService.printResponse(response, current.sum.intValue());
    }

  }

  private static class Shared {

    public /*static*/ synchronized void m1(final int ms) {
      ConcurrencyService.sleepInMilliseconds(ms);
      logger.info("m1");
    }

    public synchronized void m2(final int ms) {
      ConcurrencyService.sleepInMilliseconds(ms);
      logger.info("m2");
    }

  }

  private static class PhaserSum {

    final LongAdder sum = new LongAdder();
    final Phaser ph;

    private PhaserSum(Phaser ph) {
      this.ph = ph;
    }

    @Override
    public String toString() {
      return "PhaserSum{" +
          "sum=" + sum +
          ", ph=" + ph +
          '}';
    }
  }

  public static void main(String[] args) {

    final Shared s = new Shared();

    new Thread(() -> {
      s.m1(2000);
    }).start();

    new Thread(() -> {
      s.m2(4000);
    }).start();

  }


}
