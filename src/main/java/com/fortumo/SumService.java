package com.fortumo;

import static com.fortumo.ConcurrencyService.await;

import java.util.concurrent.BlockingDeque;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import javax.servlet.ServletOutputStream;
import javax.servlet.ServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SumService {

  private static final Logger logger = LoggerFactory.getLogger(SumService.class);

  final ReentrantLock lock = new ReentrantLock();

  //  final Deque<SumerFuture> sumerFutures = new ConcurrentLinkedDeque<>();
  final BlockingDeque<SumerFuture> sumerFutures = new LinkedBlockingDeque<>();

  final ExecutorService executorService = Executors.newFixedThreadPool(100);

  public void process(final String input, final ServletResponse response) throws Exception {

    logger.info("########## input: " + input + ", " + lock + "\n");

    if ("end".equals(input)) {

      final SumerFuture last = sumerFutures.pollLast();

      logger.info("<<<<<<< last: " + last + ", " + lock);

      if (last != null) {

        final Condition c1 = lock.newCondition();

        last.sumer.end(() -> {
          lock.lock();
          logger.info("Is about to signal.");
          c1.signal();
          logger.info("Signalled.");
          lock.unlock();
        });

        new Thread(() -> last.sumer.end(), "Sumer ender.").start();

        /*new Thread(() -> {
          while (!last.future.isDone()) {
            ConcurrencyService.sleepInMilliseconds(25);
          }
          lock.lock();
          logger.info("Is about to signal.");
          c1.signal();
          logger.info("Signalled.");
          lock.unlock();
        }, "Signaller").start();

        last.sumer.end();*/

        lock.lock();
        await(c1, "await end");

        final Integer result = last.future.get();
        printResponse(response, result);
        last.sumer.responseBlockCondition.signalAll();

        logger.info("Unlock: " + input + ", " + lock);
        lock.unlock();

      } else {
        printResponse(response, null);
      }

    } else {

      final int number = Integer.parseInt(input.replace("x", ""));
      int delay = input.endsWith("x") ? number : 0;

      try {

        lock.lock();
        SumerFuture current = sumerFutures.peekLast();
        logger.info("Current 1 = " + current + ", " + lock);

        if (current == null) {
          final Condition condition = lock.newCondition();
          final __Sumer sumer = new __Sumer(delay, condition);
          final Future<Integer> future = executorService.submit(sumer);
          current = new SumerFuture(sumer, future);
          sumerFutures.add(current);
        }

        logger.info("Current 2 = " + current + ", " + lock);
        lock.unlock();

        current.sumer.add(number);

        lock.lock();
        await(current.sumer.responseBlockCondition, input);
        printResponse(response, current.sumer.getSum());
        lock.unlock();
      } finally {
        //logger.info("Unlock: " + input + ", " + lock);
        //lock.unlock();
      }

    }

  }

  public static void printResponse(final ServletResponse resp, final Integer r) {
    try {
      if (r != null) {
        logger.info("Print response: " + r);
      }
      if (resp == null) {
        return;
      }
      final ServletOutputStream sos = resp.getOutputStream();
      final String response = r == null ? "" : r.toString();
      sos.println(response);
      sos.flush();
    } catch (final Exception e) {
      e.printStackTrace();
    }
  }


  private static class SumerFuture {

    public __Sumer sumer;
    //    public Sumer sumer;
    public Future<Integer> future;

    public SumerFuture(final __Sumer sumer, final Future<Integer> future) {
      this.sumer = sumer;
      this.future = future;
    }

    @Override
    public String toString() {
      return "SumerFuture{" +
          "sumer=" + sumer +
          ", future=" + future +
          '}';
    }
  }

}
