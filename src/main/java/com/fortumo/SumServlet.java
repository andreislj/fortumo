package com.fortumo;

import java.io.IOException;
import java.util.stream.Collectors;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@WebServlet(name = "SumServlet", urlPatterns = {"/"}, loadOnStartup = 1)
public class SumServlet extends HttpServlet {

  private static final Logger logger = LoggerFactory.getLogger(SumServlet.class);

  private SumService sumService = new SumService();

  protected void doPost(final HttpServletRequest request, final HttpServletResponse response) throws IOException {

    final String input = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
    final Thread currentThread = Thread.currentThread();
    currentThread.setName("******req-" + input + "******");

    try {
      sumService.process(input, response);
    } catch (final Exception e) {
      e.printStackTrace();
    }

  }

}
