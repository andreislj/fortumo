package com.fortumo;

import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.ServletHandler;
import org.eclipse.jetty.util.thread.QueuedThreadPool;
import org.eclipse.jetty.util.thread.ThreadPool;

public class JettyServer {

  private Server server;

  public void start(final int port, final int minThreads, final boolean isJoin) throws Exception {
    final ThreadPool threadPool = new QueuedThreadPool(400, minThreads);
    server = new Server(threadPool);
    final ServerConnector connector = new ServerConnector(server);
    connector.setPort(port);
    server.setConnectors(new Connector[]{connector});

    final ServletHandler servletHandler = new ServletHandler();
    servletHandler.addServletWithMapping(SumServlet.class, "/");

    server.setHandler(servletHandler);

    server.start();

    if (isJoin) {
      server.join();
    }


  }

  public void stop() {
    try {
      server.stop();
    } catch (final Exception e) {
      e.printStackTrace();
    }
  }

  public static void main(String[] args) throws Exception {
    final JettyServer jettyServer = new JettyServer();
    jettyServer.start(1337, 20, true);
  }

}

